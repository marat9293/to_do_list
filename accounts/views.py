from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from .forms import MyUserCreationForm

def login_view(request):
    context = {}
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)
            next = request.GET.get('next')
            if next:
                return redirect(next)
            return redirect('home_page')
        else:
            context['has_error'] = True   
    else:
        return render(request, 'accounts/login.html', context=context)
    

def logout_view(request):
    logout(request)
    return redirect('home_page')


def register_view(request):
    if request.method == 'POST':
        form = MyUserCreationForm(data=request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect('home_page')
    else:
        form = MyUserCreationForm()
    return render(request, 'accounts/register.html', context={'form': form})

    

# class UserDetailView(LoginRequiredMixin, DetailView):
#     model = get_user_model()
#     template_name = 'accounts/user_detail.html'
#     context_object_name = 'user_obj'
#     paginate_related_by = 5
#     paginate_realted_orphans = 0

#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         posts = self.object.posts.order_by('-created_at')
#         paginator = Paginator(posts, self.paginate_related_by, orphans=self.paginate_realted_orphans)
#         page_number = self.request.GET.get('page', 1)
#         page = paginator.get_page(page_number)
#         context['page_obj'] = page
#         context['post'] = page.object_list
#         context['is_paginated'] = page.has_other_pages()
#         return context
