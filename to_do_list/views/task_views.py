from django.shortcuts import render, redirect, get_object_or_404
from ..models import Task, Status
# from ..forms import TaskForm

def index_view(request):
    return render(request, 'index.html')


def task_view(request, *args, **kwargs):
    task = get_object_or_404(Task, pk=kwargs.get('pk'))
    return render(request, 'task_view.html', context={
        'task': task
    })

def task_list_view(request):
    tasks = Task.objects.all()
    status = Status.objects.all()
    return render(request, 'task_list.html', context={
        'tasks': tasks,
        'status': status
    })


def task_create_view(request):
    if not request.user.is_authenticated:
            return redirect('login')
    if request.method == 'GET':
        return render(request, 'task_create.html', context={
            'status': Status.objects.all()
        })
    elif request.method == 'POST':
        status = Status.objects.get(pk=request.POST.get('status'))
        task = Task.objects.create(
            title=request.POST.get('title'),
            description=request.POST.get('description'),
            status=status,
        )
        return redirect('task_list')

# def task_create_view(request):
#     if request.method == 'GET':
#         form = TaskForm()
#         return render(request, 'task_create.html', context={
#             'forms': form})
#     elif request.method == 'POST':
#         form = TaskForm(request.POST)
#         if form.is_valid():
#             new_task = Task.objects.create(
#             title=form.cleaned_data['title'],
#             description=form.cleaned_data['description'],
#             status=form.cleaned_data['status'],
#         )
#             return redirect('home_page')
#         else:
#             return render(request, 'task_create.html', context={
#                 'form': form
#             })
        
    

def task_update_view(request, *args, **kwargs):
    if not request.user.is_authenticated:
            return redirect('login')
    task = get_object_or_404(Task, pk=kwargs.get('pk'))
    if request.method == 'GET':
        return render(request, 'task_update.html', context={
            'task': task,
            'status': Status.objects.all()
        })
    elif request.method == 'POST':
        status = Status.objects.get(pk=request.POST.get('status'))
        task.title = request.POST.get('title')
        task.description = request.POST.get('description')
        task.status = status
        task.save()
        return redirect('task_detail', pk=task.pk)
    

def task_delete_view(request, *args, **kwargs):
    if not request.user.is_authenticated:
            return redirect('login')
    task = get_object_or_404(Task, pk=kwargs.get('pk'))
    task.delete()
    return redirect('home_page')