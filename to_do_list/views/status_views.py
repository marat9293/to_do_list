from django.shortcuts import render, redirect
from..models import Status


def status_create_view(request):
    if request.method == 'GET':
        return render(request, 'status_create.html')
    elif request.method == 'POST':
        status = Status.objects.create(
            status=request.POST.get('status')
        )
        return redirect('status_list')
    

def status_list_view(request):
    return render(request, 'status_list.html', context = {
        'status': Status.objects.all()
    })