from django.contrib import admin
from .models import Task, Status


class TaskAdmin(admin.ModelAdmin):
    list_display = ['title', 'status', 'created_at']
    list_filter = ['status']
    search_fields = ['title']
    readonly_fields = ['created_at', 'updated_at']

class StatusAdmin(admin.ModelAdmin):
    list_display = ['status']

admin.site.register(Status, StatusAdmin)
admin.site.register(Task, TaskAdmin)