from django.urls import path
from to_do_list.views.status_views import (
    status_create_view,
    status_list_view
)


urlpatterns = [
    path('create', status_create_view, name='status_create'),
    path('list/', status_list_view, name='status_list'),
]