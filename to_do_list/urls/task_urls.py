from django.urls import path
from to_do_list.views.task_views import (
    task_view, 
    task_create_view,
    task_list_view,
    task_update_view,
    task_delete_view
)


urlpatterns = [
    path('detail/<int:pk>', task_view, name='task_detail'),
    path('create', task_create_view, name='task_create'),
    path('list/', task_list_view, name='task_list'),
    path('update/<int:pk>', task_update_view, name='task_update'),
    path('delete/<int:pk>/', task_delete_view, name='task_delete')
]