from django.db import models


class Status(models.Model):
    status = models.CharField(max_length=100, blank=False, verbose_name='Статус задачи')


class Task(models.Model):
    title = models.CharField(max_length=200, null=False, blank=False, verbose_name='Заголовок')
    description = models.CharField(max_length=2000, blank=False, verbose_name='Описание задачи')
    status = models.ForeignKey(Status, on_delete=models.CASCADE, null=False, verbose_name='Текущий статус', related_name='tasks')
    created_at = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания')
    updated_at = models.DateTimeField(auto_now=True, verbose_name='Дата обновления')

    def __str__(self):
        return f"-{self.pk} - {self.title}"