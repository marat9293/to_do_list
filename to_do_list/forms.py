from django import forms
from django.forms import widgets
from .models import Status, Task


# class TaskForm(forms.Form):
#     title = forms.CharField(max_length=200, required=True, label='Название задач')
#     description = forms.CharField(max_length=2000, required=False, label='Описание задачи', widget=widgets.Textarea)
#     status = forms.CharField(max_length=100, required=True, label='Статус задачи')
#     # created_at = forms.DateTimeField(auto_now_add=True, label='Дата создания')
#     # updated_at = forms.DateTimeField(auto_now=True, label='Дата обновления')

class StatusForm(forms.Form):
    status = forms.CharField(max_length=30, required=True, label='Название статуса', widget=widgets.TextInput())